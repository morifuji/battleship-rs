use crate::coordinate::Coordinate;

#[derive(Clone)]
pub struct Ship {
    // TODO 本当はCellにしたいが循環参照しそうなので数値に
    coordinate_list: Vec<Coordinate>
}

impl Ship {
    pub fn display(is_sunked: bool) -> String {
        if is_sunked {
            return "X".to_string();
        }
        "x".to_string()
    }

    pub fn new(coordinate: Vec<Coordinate>) -> Ship {
        Ship {
            coordinate_list: coordinate
        }
    }

    pub fn get_coordinate_list(&self) -> Vec<Coordinate> {
        self.coordinate_list.clone()
    }
}

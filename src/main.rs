extern crate termion;
#[macro_use]
extern crate log;

use std::io::{stdin, stdout, Write};
use termion::event::{Event, Key, MouseEvent};
use termion::input::{MouseTerminal, TermRead};
use termion::raw::IntoRawMode;
use termion::{color, cursor};

pub mod board;
pub mod cell;
pub mod coordinate;
pub mod direction;
pub mod game;
pub mod random;
pub mod ship;

use game::Event as GamenEvent;

pub const BOARD_SIZE: u16 = 7;

fn main() {
    env_logger::init();
    let stdin = stdin();

    let mut game = game::Game::new();
    let mut viewer = Viewer::new();

    // 船の配置
    // ランダムに
    game.prepare();

    viewer.display(&mut game, &mut None);
    info!("start");

    for c in stdin.events() {
        let evt = c.unwrap();
        match evt {
            Event::Key(Key::Char('q')) => break,
            Event::Mouse(me) => match me {
                MouseEvent::Press(_, x, y) => {
                    viewer.reset_cursol();

                    if x.checked_sub(viewer.window_offset_x).is_none()
                        || y.checked_sub(viewer.window_offset_y).is_none()
                    {
                        info!("範囲外ですね");
                        viewer.display(&mut game, &mut None);
                        continue;
                    }

                    let result = game.input(x - viewer.window_offset_x, y - viewer.window_offset_y);
                    if !result {
                        info!("もう一度！");
                        viewer.display(&mut game, &mut None);
                        continue;
                    }

                    let event = &mut game.find_event();
                    viewer.display(&mut game, event);
                    match event {
                        Some(GamenEvent::YourAreWin) => return,
                        Some(GamenEvent::YourAreLose) => return,
                        _ => (),
                    };

                    // 相手のターン
                    game.enemy_input();
                    let event = &mut game.find_event();
                    viewer.display(&mut game, event);
                    match event {
                        Some(GamenEvent::YourAreWin) => return,
                        Some(GamenEvent::YourAreLose) => return,
                        _ => (),
                    };
                }
                _ => (),
            },
            _ => {}
        }
    }
}

pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;

struct Viewer {
    stdout: StdoutType,

    window_offset_x: u16,
    window_offset_y: u16,
}

impl Viewer {
    fn new() -> Viewer {
        let mut stdout = MouseTerminal::from(stdout().into_raw_mode().unwrap());
        write!(
            stdout,
            "{}{}",
            termion::clear::All,
            termion::cursor::Goto(1, 1)
        )
        .unwrap();
        Viewer {
            stdout,
            window_offset_x: 10,
            window_offset_y: 10,
        }
    }

    fn display(&mut self, game: &mut game::Game, event: &mut Option<GamenEvent>) {
        game.display(&mut self.stdout, self.window_offset_x, self.window_offset_y);
        match event {
            Some(GamenEvent::YourAreWin) => {
                let content = "congulatulation!!";
                write!(
                    self.stdout,
                    "{}{}{}{}{}{}",
                    cursor::Goto(
                        self.window_offset_x + BOARD_SIZE - (content.len() as u16 / 2),
                        self.window_offset_y + BOARD_SIZE + 2
                    ),
                    color::Fg(color::Blue),
                    color::Bg(color::Yellow),
                    content,
                    color::Fg(color::Reset),
                    color::Bg(color::Reset),
                )
                .expect(&format!(
                    "イベントメッセージ「{}」の表示に失敗",
                    content.to_string()
                ));
            }
            Some(GamenEvent::YourAreLose) => {
                let content = "you lose....";
                write!(
                    self.stdout,
                    "{}{}{}{}{}{}",
                    cursor::Goto(
                        self.window_offset_x + BOARD_SIZE - (content.len() as u16 / 2),
                        self.window_offset_y + BOARD_SIZE + 2
                    ),
                    color::Fg(color::Blue),
                    color::Bg(color::Yellow),
                    content,
                    color::Fg(color::Reset),
                    color::Bg(color::Reset),
                )
                .expect(&format!(
                    "イベントメッセージ「{}」の表示に失敗",
                    content.to_string()
                ));
            }
            _ => (),
        }
        self.stdout.flush().unwrap();
    }

    fn reset_cursol(&mut self) {
        write!(
            self.stdout,
            "{}{}",
            termion::clear::All,
            cursor::Goto(
                self.window_offset_x + BOARD_SIZE + 10,
                self.window_offset_y + BOARD_SIZE + 10
            )
        )
        .expect("リセットに失敗しました");
        self.stdout.flush().unwrap();
    }
}

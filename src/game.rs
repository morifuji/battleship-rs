use crate::board::{Board, CellTypeWithCoordinate};
use termion::input::MouseTerminal;
pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;

#[derive(Clone)]
pub struct Game {
    pub my_board: Board,
    pub enemy_board: Board,
    window_offset_x: u16,
    window_offset_y: u16,
}

#[derive(Clone, Copy)]
pub enum Event {
    YourAreWin,
    YourAreLose
}

impl Game {
    pub fn new() -> Game {
        Game {
            my_board: Board::new(),
            enemy_board: Board::new(),
            // 相手と自分のviewの差
            window_offset_x: 21,
            window_offset_y: 0,
        }
    }

    pub fn prepare(&mut self) {
        self.my_board.prepare();
        self.enemy_board.prepare();
    }

    pub fn display(&mut self, stdout: &mut StdoutType, offset_x: u16, offset_y: u16) {
        self.my_board.display(stdout, offset_x, offset_y);
        self.enemy_board
            .display(stdout, offset_x + self.window_offset_x, offset_y + self.window_offset_y);
    }

    /**
     * 入力が失敗したら
     * TODO 複数考えるならVec？
     */
    pub fn input(&mut self, x: u16, y: u16) -> bool {
        if x.checked_sub(self.window_offset_x).is_none() || y.checked_sub(self.window_offset_y).is_none() {
            info!("ゲームの範囲外ですね");
            return false;
        }
        self.enemy_board.attack(x - self.window_offset_x, y - self.window_offset_y)
    }

    pub fn find_event(&mut self) -> Option<Event> {
        if self.my_board.is_losed_board() {
            return Some(Event::YourAreLose);
        }
        if self.enemy_board.is_losed_board() {
            return Some(Event::YourAreWin);
        }

        None
    }

    pub fn enemy_input(&mut self) {
        let my_board = &mut self.my_board;

        let CellTypeWithCoordinate {cell: _, x, y } =  my_board.clone().search_target_cell();

        my_board.attack(x.clone(), y.clone());
    }
}

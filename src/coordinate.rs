#[derive(Copy, Clone, Hash, Eq, Debug)]
pub struct Coordinate {
    pub x: u16,
    pub y: u16,
}


impl PartialEq for Coordinate {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y 
    }
}
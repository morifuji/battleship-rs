use crate::BOARD_SIZE;
use crate::coordinate::Coordinate;
use crate::direction::Direction;

use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

/**
 * 失敗した場合はNone
 */
pub fn pick_random_position(size: u16) -> Option<Vec<Coordinate>>{
    let mut coordinate_list = vec!();
    let head = pick_random_cell();
    let direction = pick_random_direction();
    coordinate_list.push(head);
    for _ in 0..size-1 { 

        let last_coordinate = coordinate_list.last().unwrap().clone();
        match direction {
            Direction::Top => {
                if last_coordinate.y +1 == BOARD_SIZE {
                    return None
                }
                coordinate_list.push(Coordinate {
                    x: last_coordinate.x,
                    y: last_coordinate.y+1,
                })
            },
            Direction::Right => {
                if last_coordinate.x +1 == BOARD_SIZE {
                    return None
                }
                coordinate_list.push(Coordinate {
                    x: last_coordinate.x+1,
                    y: last_coordinate.y,
                })
            },
            Direction::Bottom => {
                if last_coordinate.y.checked_sub(1).is_none() {
                    return None
                }
                coordinate_list.push(Coordinate {
                    x: last_coordinate.x,
                    y: last_coordinate.y-1,
                })
            },
            Direction::Left => {
                if last_coordinate.x.checked_sub(1).is_none() {
                    return None
                }
                coordinate_list.push(Coordinate {
                    x: last_coordinate.x-1,
                    y: last_coordinate.y,
                })
            },
        }
     }

    Some(coordinate_list)
}

pub fn pick_random_cell() -> Coordinate{
    let mut rng = rand::thread_rng();
    let x = rng.gen_range(0, BOARD_SIZE);
    let mut rng = rand::thread_rng();
    let y = rng.gen_range(0, BOARD_SIZE);

    Coordinate {
        x: x,
        y: y,
    }
}

pub fn pick_random_direction() -> Direction{
    let direction: Direction = rand::random();
    direction
}
impl Distribution<Direction> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Direction {
        match rng.gen_range(0, 4) {
            0 => Direction::Top,
            1 => Direction::Right,
            2 => Direction::Bottom,
            3 => Direction::Left,
            x => panic!(format!("ランダムの範囲がおかしい. x = {}", x))
        }
    }
}

#[test]
fn test_pick_random_position() {
    let position = pick_random_position(1);
    assert_ne!(position, None);

    // 必ず失敗
    let position = pick_random_position(BOARD_SIZE + 1);
    assert_eq!(position, None);
}


#[test]
fn test_pick_random_cell() {
    for _ in 0..50000 {
        let cell = pick_random_cell();
        assert_eq!((0..BOARD_SIZE).contains(&cell.x), true);
        assert_eq!((0..BOARD_SIZE).contains(&cell.y), true);
    }
}



#[test]
fn test_pick_random_direction() {
    let mut top_count = 0;
    let mut right_count = 0;
    let mut bottom_count = 0;
    let mut left_count = 0;
    for _ in 0..50000 {
        let direction = pick_random_direction();
        if let Direction::Top = direction {
            top_count = top_count+1;
        }
        if let Direction::Right = direction {
            right_count = right_count+1;
        }
        if let Direction::Bottom = direction {
            bottom_count = bottom_count+1;
        }
        if let Direction::Left = direction {
            left_count = left_count+1;
        }
    }

    println!("{},{},{},{}", top_count, right_count, bottom_count, left_count);

    assert_eq!(top_count > 6000, true); 
    assert_eq!(right_count > 6000, true); 
    assert_eq!(bottom_count > 6000, true); 
    assert_eq!(left_count > 6000, true); 
}
use crate::ship::Ship;
use termion::{color};
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Clone)]
enum CellType {
    Unknown(Option<Rc<RefCell<Ship>>>),
    Empty,
    InShip(Rc<RefCell<Ship>>),
}

#[derive(Clone)]
pub struct Cell {
    cell_type: CellType,
}

impl Default for Cell {
    fn default() -> Self {
        Cell::new()
    }
}

impl Cell {
    pub fn new() -> Cell {
        Cell {
            cell_type: CellType::Unknown(None),
        }
    }

    pub fn set_ship(&mut self, ship: Rc<RefCell<Ship>>)  {
        self.cell_type = CellType::Unknown(Some(ship));
    }

    pub fn can_attack(&mut self) -> bool {
        if let CellType::Unknown(_) = self.cell_type {
            return true;
        }

        false
    }

    pub fn is_in_ship(&mut self) -> bool {
        if let CellType::InShip(_) = self.cell_type {
            return true;
        }

        false
    }

    pub fn attack(&mut self) -> bool {
        let cloned = self.clone();
        match cloned.cell_type {
            CellType::Unknown(ship) => {
                self.cell_type = if ship.is_none() {
                    CellType::Empty
                } else {
                    CellType::InShip(ship.unwrap())
                };
                true
            },
            _ => false
        }
    }

    pub fn display(self) -> String {
        match self.cell_type {
            CellType::InShip(_) => format!("{}{}", color::Fg(color::Yellow), Ship::display(false)),
            CellType::Empty => " ".to_string(),
            CellType::Unknown(_) => format!("{}?", color::Fg(color::White))
        }
    }
}

#[test]
fn test_can_attack() {
    let mut cell_empty = Cell {
        cell_type: CellType::Empty
    };
    let mut cell_unknown_with_none = Cell {
        cell_type: CellType::Unknown(None)
    };
    
    assert_eq!(cell_empty.can_attack(), false);
    assert_eq!(cell_empty.attack(), false);

    assert_eq!(cell_unknown_with_none.can_attack(), true);
    assert_eq!(cell_unknown_with_none.attack(), true);
    // 2回目は洗濯できない
    assert_eq!(cell_unknown_with_none.attack(), false);
}
use crate::cell::{Cell };
use crate::ship::Ship;
use crate::BOARD_SIZE;
use std::io::Write;
use termion::input::MouseTerminal;
use termion::{color, cursor};
pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;

use crate::random;

const BOARD_SIZE_USIZED: usize = BOARD_SIZE as usize;

use crate::coordinate::Coordinate;
use std::collections::HashSet;
use std::rc::Rc;
use std::cell::RefCell;

pub type CellsType = [[Cell; BOARD_SIZE_USIZED]; BOARD_SIZE_USIZED];

#[derive(Clone)]
pub struct CellTypeWithCoordinate {
    pub cell: Cell,
    pub x: u16,
    pub y: u16
}

#[derive(Clone)]
pub struct Board {
    pub cells: CellsType,
    ships: Option<Vec<Rc<RefCell<Ship>>>>,
}

impl Board {
    pub fn new() -> Board {

        // link: https://tyfkda.github.io/blog/2020/03/19/rust-init-array.html
        let cells: CellsType = Default::default();

        Board {
            cells,
            ships: None,
        }
    }

    pub fn get_cells(self) -> CellsType {
        self.cells.clone()
    }

    pub fn prepare(&mut self) {
        let ship_length_list: Vec<u16> = vec![2, 1];

        let cell_count: u16 = ship_length_list.iter().sum();
        info!("cell_count: {}", cell_count);

        let mut coordinate_list: Vec<Vec<Coordinate>>;
        loop {
            let coordinate_result_list: Vec<Option<Vec<Coordinate>>> = ship_length_list
                .clone()
                .into_iter()
                .map(|v| random::pick_random_position(v))
                .collect();
            let is_valid = coordinate_result_list.iter().all(|v| v.is_some());
            if !is_valid {
                continue;
            }

            coordinate_list = coordinate_result_list
                .into_iter()
                .map(|v| v.unwrap())
                .collect();
            let concated = coordinate_list.concat();

            let unique_set: HashSet<Coordinate> = concated.into_iter().collect();
            info!("uniquecount: {}", unique_set.len());
            if unique_set.len() == (cell_count as usize) {
                break;
            }
        }

        coordinate_list.into_iter().for_each(|v| {
            self.add_ship(v);
        });
    }

    fn add_ship(&mut self, coordinate_list: Vec<Coordinate>) {
        let ship = Rc::new(RefCell::new(Ship::new(coordinate_list.clone())));
        for i in 0..coordinate_list.len() {
            let x = coordinate_list[i].x as usize;
            let y = coordinate_list[i].y as usize;
            self.cells[y][x].set_ship(Rc::clone(&ship));
        }
        if self.ships.is_none() {
            self.ships = Some(vec![]);
        }

        self.ships.as_mut().unwrap().push(ship);
    }

    pub fn display(&mut self, stdout: &mut StdoutType, offset_x: u16, offset_y: u16) {
        // セルの描写
        self.cells.iter().enumerate().for_each(|(i, v)| {
            v.iter().enumerate().for_each(|(i2, v2)| {
                write!(
                    stdout,
                    "{}{}{}",
                    cursor::Goto(i2 as u16 + offset_x, i as u16 + offset_y),
                    v2.clone().display(),
                    cursor::Goto(offset_x + BOARD_SIZE + 10, offset_y + BOARD_SIZE + 10)
                )
                .expect("マップの表示に失敗");
            });
        });

        // 船の描写
        self.ships.clone().unwrap().into_iter().for_each(|ship| {
            if self.is_sunked_ship(&ship.borrow()) {
                ship.borrow().get_coordinate_list().iter().for_each(|c| {
                    write!(
                        stdout,
                        "{}{}{}{}",
                        cursor::Goto(c.x + offset_x, c.y + offset_y),
                        color::Fg(color::Red),
                        Ship::display(true),
                        cursor::Goto(offset_x + BOARD_SIZE + 10, offset_y + BOARD_SIZE + 10)
                    )
                    .expect("マップの表示に失敗");
                })
            }
        });
    }

    pub fn is_losed_board(&mut self) -> bool {
        self.ships.clone().unwrap().iter().all(|ship| {
            self.is_sunked_ship(&ship.borrow())
        })
    }

    fn in_board(&mut self, x: u16, y: u16) -> bool {
        x < BOARD_SIZE && y < BOARD_SIZE
    }

    pub fn attack(&mut self, x: u16, y: u16) -> bool {
        if !self.in_board(x, y) {
            info!("ボードの外ですね!");
            return false;
        }

        let x_usize = x as usize;
        let y_usize = y as usize;
        debug!("{}, {}", x_usize, y_usize);
        let cell = &mut self.cells[y_usize][x_usize];
        if !cell.can_attack() {
            info!("そこはアタックできません");
            return false;
        }

        let result = cell.attack();
        if !result {
            info!("エラー");
            return false;
        }

        true
    }

    pub fn is_sunked_ship(&mut self, ship: &Ship) -> bool {
        ship.get_coordinate_list().iter().all(|v| {
            let y = v.y as usize;
            let x = v.x as usize;
            self.cells[y][x].is_in_ship()
        })
    }

    fn get_cell(self, x: u16, y: u16) -> Cell {
        let cells = self.get_cells();
        cells[y as usize][x as usize].clone()
    }

    pub fn search_target_cell(self) -> CellTypeWithCoordinate {
        loop {
            let coordinate = random::pick_random_cell();
            let mut target_cell = self.clone().get_cell(coordinate.x, coordinate.y);

            if (&mut target_cell).can_attack() {
                return  CellTypeWithCoordinate{
                    cell: target_cell,
                    x: coordinate.x,
                    y: coordinate.y
                };
            }
        }
    }
}

#[test]
fn test_in_board() {
    let mut board = Board::new();
    board.prepare();

    let mut board_cloned = (&mut board).clone();

    // 最初は沈んでいる船は一切ない
    let sunked_ship_is_none = board
        .ships
        .clone()
        .unwrap()
        .iter()
        .all(|s| !board.is_sunked_ship(&s.borrow()));
    assert_eq!(sunked_ship_is_none, true);

    board_cloned
        .cells
        .clone()
        .iter()
        .enumerate()
        .for_each(|(i, v)| {
            v.iter().enumerate().for_each(|(i2, _)| {
                board_cloned.attack(i2 as u16, i as u16);
            });
        });

    // 全部沈んでいるかどうか
    let shiped_cloned = board_cloned.ships.clone();
    let is_all_ship_sunked = shiped_cloned.unwrap().iter().all(|s| {
        assert_eq!(board_cloned.is_sunked_ship(&s.borrow()), true);
        board_cloned.is_sunked_ship(&s.borrow())
    });

    assert_eq!(is_all_ship_sunked, true);
}

#[test]
fn test_is_sunked_ship() {
    let mut board = Board::new();
    board.prepare();

    assert_eq!(board.in_board(0, 0), true);
    assert_eq!(board.in_board(1, 0), true);
    assert_eq!(board.in_board(0, 1), true);
    assert_eq!(board.in_board(BOARD_SIZE - 1, BOARD_SIZE - 1), true);
    assert_eq!(board.in_board(BOARD_SIZE - 1, BOARD_SIZE), false);
    assert_eq!(board.in_board(BOARD_SIZE, BOARD_SIZE - 1), false);
}

#[test]
fn test_unique() {
    let mut board = Board::new();
    assert_eq!(board.ships.is_none(), true);

    board.add_ship(vec!(
        Coordinate{
            x: 0,
            y: 1
        },
        Coordinate{
            x: 0,
            y: 2
        },
        Coordinate{
            x: 0,
            y: 3
        }
    ));
    assert_eq!(board.ships.clone().unwrap().len(), 1);


    board.add_ship(vec!(
        Coordinate{
            x: 1,
            y: 1
        },
        Coordinate{
            x: 1,
            y: 2
        },
        Coordinate{
            x: 1,
            y: 3
        }
    ));
    assert_eq!(board.ships.clone().unwrap().len(), 2);
}

